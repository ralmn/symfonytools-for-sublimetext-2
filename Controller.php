<?php

namespace  %s\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\SecurityContext;


/**
*
* @Route("/")
*
*/
class %sController extends Controller
{
    /**
     * @Route("/", name="")
     * @Template()
     */
    public function indexAction(){
    	return array();
    }

  }
