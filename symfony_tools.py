import sublime, sublime_plugin, os, json

class SymfonyToolsCommand(sublime_plugin.WindowCommand):

	bundle = ""
	packagePath =""
	settings = json.loads("{}")
	def run(self):
		messages = ["Controller", "View"]
		window = self.window
		self.getSettingsFile()
		self.packagePath = os.path.join(sublime.packages_path(), "SymfonyTools")
	  	window.show_quick_panel(messages, self.onItemSelected, sublime.MONOSPACE_FONT)

	def onItemSelected(self, number):
		window = self.window
		if (number == 0):
			sublime.status_message("Create controller")
			window.show_input_panel("Controller Name", "Default", self.onControllerNameSet, None, None)
		elif(number == 1):
			self.viewfirst()

	def onControllerNameSet(self, text):
		sublime.status_message("Creation du controller : "  +text)
		src = self.getSrc()
		dir  = src + self.bundle + "/Controller/"
		f = dir + text + "Controller.php"
		b = self.bundle
		content = self.getControllerModel()
		content = content % (b , text)
		if(not os.path.isfile(f)):
			if (not os.path.exists(dir)):
				os.makedirs(dir)
			file = open(f, "w")
			file.write(content)
			file.close()
		else:
			sublime.error_message("File already exist")
		self.window.open_file(f)

	def getSrc(self):
		window = self.window
		folders = window.folders()
		for folder in folders:
			dirEntries = os.listdir(folder)
			for entry in dirEntries:
				if (entry == "src"):
					dir = folder + "/src/"
					return dir


	def getControllerModel(self):
		path = os.path.join(self.packagePath , "Controller.php")
		if(not os.path.isfile(path)):
			sublime.error_message(path + " not found")

		c = open(path).read()

		return str(c)

	def getSettingsModel(self):
		path = os.path.join(self.packagePath , "symfony_tools.json")
		if(not os.path.isfile(path)):
			sublime.error_message(path + " not found")

		c = open(path).read()

		return str(c)
	
	def initSettings(self):
		self.createSettings()
		

	def getSettingsFile(self):
		folders = self.window.folders()
		for folder in folders:
			dirEntries = os.listdir(folder)
			for entry in dirEntries:
				if (entry == "symfony_tools.json"):
					f = os.path.join(folder, "symfony_tools.json")
					j = open(f).read()
					self.settings = json.loads(j)
					self.bundle = self.settings['bundle']
					return f

		self.createSettings()
	

	def createSettings(self):
		window = self.window
		window.show_input_panel("Bundle", "Name/NameBunde", self.haveBundleName,None,None)

	def haveBundleName(self, text):
		self.settings['bundle'] = text
		sublime.error_message(text)
		self.saveSettings()

	def saveSettings(self):
		settings = self.settings
		self.bundle = settings['bundle']
		folders = self.window.folders()
		f = folders[0]
		c = self.getSettingsModel() % (self.bundle)
		file = open(f +"/symfony_tools.json", "w")
		file.write(c)
		file.close()
		return f + "/symfony_tools.json"


	def getController(self):
		src = self.getSrc()
		ctrldir  = src + self.bundle + "/Controller/"
		cdir = os.listdir(ctrldir)
		controllers = []
		for f in cdir:
			if(not f.endswith(".php")):
				for df in os.listdir(ctrldir + f + "/"):
					if(df.endswith(".php")):
						ddf  = f + "/" + df
						nf = ddf.replace("Controller.php", "")
						controllers.append(nf)
			else:
				nf = f.replace("Controller.php", "")
				controllers.append(nf)

		return controllers


	def viewfirst(self):
		controller = self.getController()
		self.window.show_quick_panel(controller, self.onControllerSel, sublime.MONOSPACE_FONT)

	def onControllerSel(self, number):
		controllers = self.getController()
		controller = controllers[number]
		self.controller = controller
		engine = ["php", "twig"]
		self.window.show_quick_panel(engine, self.onEngineSel, sublime.MONOSPACE_FONT)

	def onEngineSel(self, number):
		engines = ["php", "twig"]
		engine = engines[number]
		self.engine = engine
		self.window.show_input_panel("View name", "index", self.onViewNameSet, None, None)

	def onViewNameSet(self, name):
		controller = self.controller
		engine = self.engine
		self.createView(controller, name, engine)

	def getViewModel(self, engine):
		n = "model.html." + engine
		path = os.path.join(self.packagePath , n)
		if(not os.path.isfile(path)):
			sublime.error_message(path + " not found")
		c = open(path).read()

		return str(c)

	def createView(self, controller, name, engine):

		file_name = name + ".html." + engine
		b = self.getSrc() + self.bundle
		resdir = b + "/Ressources/views/" + controller +"/"
		fn = os.path.join(resdir, file_name)
		content = self.getViewModel(engine)
		if(not os.path.isfile(fn)):
			if (not os.path.exists(resdir)):
				os.makedirs(resdir)
			file = open(fn, "w")
			file.write(content)
			file.close()
		else:
			sublime.error_message("File already exist")
		self.window.open_file(fn)
